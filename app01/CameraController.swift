//
//  CameraController.swift
//  app01
//
//  Created by Luis Romo on 7/3/19.
//  Copyright © 2019 Luis Romo. All rights reserved.
//

import UIKit
import AVFoundation

class CameraController: UIViewController, AVCapturePhotoCaptureDelegate{
    
    let dismisBtn:UIButton = {
        let btn = UIButton(type: .system)
        //        btn.setImage(#imageLiteral(resourceName: "right_arrow_shadow"), for: .normal)
        btn.setTitle("Cancel", for: .normal)
        btn.addTarget(self, action: #selector(handleDismiss), for: .touchUpInside)
        return btn
    }()
    @objc func handleDismiss() {
        dismiss(animated: true, completion: nil)
    }
    let captureButton:UIButton = {
        let btn = UIButton(type: .system)
        btn.setImage(#imageLiteral(resourceName: "capture_photo"), for: .normal)
        //        btn.setTitle("Capture Image", for: .normal)
        btn.addTarget(self, action: #selector(handleCapturePhoto), for: .touchUpInside)
        return btn
    }()
    
    //    override var supportedInterfaceOrientations: UIInterfaceOrientationMask{
    //        get{
    //            return  .portrait
    //        }
    //    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupCaptureSesion()
        detectOrientation()
        
        view.addSubview(captureButton)
        view.addSubview(dismisBtn)
        captureButton.anchor(top: nil, leading: nil, bottom: view.bottomAnchor, trailing: nil, padding: .init(top: 0, left: 0, bottom: 12, right: 0), size: .init(width: 80, height: 80))
        captureButton.centerXInSuperview()
        dismisBtn.anchor(top: view.topAnchor, leading: nil, bottom: nil, trailing: view.trailingAnchor, padding: .init(top: 12, left: 0, bottom: 0, right: 12), size: .init(width: 50, height: 50))
    }
    override var prefersStatusBarHidden: Bool{
        return true
    }
    @objc func handleCapturePhoto() {
        
        let settings = AVCapturePhotoSettings()
        guard let previewFormatType = settings.availablePreviewPhotoPixelFormatTypes.first else{return}
        settings.previewPhotoFormat = [kCVPixelBufferPixelFormatTypeKey as String: previewFormatType]
        output.capturePhoto(with: settings, delegate: self)
        
    }
    func photoOutput(_ output: AVCapturePhotoOutput, didFinishProcessingPhoto photo: AVCapturePhoto, error: Error?) {
        let imageData = photo.fileDataRepresentation()
        
        let previewImage = UIImage(data: imageData!)
        let containerView = PreviewPhotoContairnerView()
        containerView.previewImageView.backgroundColor = .white
        containerView.previewImageView.image = UIImage(cgImage: (previewImage?.cgImage)!, scale: previewImage!.scale, orientation: orientation!)
        
        view.addSubview(containerView)
        containerView.anchor(top: view.topAnchor, leading: view.leadingAnchor, bottom: view.bottomAnchor, trailing: view.trailingAnchor)
        
    }
    
    let output = AVCapturePhotoOutput()
    var previewLayer: AVCaptureVideoPreviewLayer?
    fileprivate func setupCaptureSesion() {
        let captureSession = AVCaptureSession()
        
        guard let captureDevice = AVCaptureDevice.default(for: .video)else{return}
        do{
            let input = try AVCaptureDeviceInput(device: captureDevice)
            if captureSession.canAddInput(input){
                captureSession.addInput(input)
            }
        }catch let err{
            print("no setup camera", err)
        }
        
        if captureSession.canAddOutput(output){
            captureSession.addOutput(output)
        }
        
        previewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
        previewLayer?.frame = view.frame
        view.layer.addSublayer(previewLayer ?? AVCaptureVideoPreviewLayer())
        
        captureSession.startRunning()
    }
    
    func detectOrientation() {
        if UIDevice.current.orientation.isLandscape {
            print("Landscape")
            // do your stuff here for landscape
        } else {
            print("Portrait")
            // do your stuff here for portrait
        }
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        detectOrientation()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        if let previewLayerConnection =  self.previewLayer?.connection, previewLayerConnection.isVideoOrientationSupported {
            updatePreviewLayer(layer: previewLayerConnection, orientation: UIApplication.shared.statusBarOrientation.videoOrientation)
        }
    }
    
    private func updatePreviewLayer(layer: AVCaptureConnection, orientation: AVCaptureVideoOrientation) {
        layer.videoOrientation = orientation
        previewLayer?.frame = self.view.bounds
    }
    
}

var orientation : UIImage.Orientation?

extension UIInterfaceOrientation {
    
    public var videoOrientation: AVCaptureVideoOrientation {
        switch self {
        case .portrait:
            orientation = .right
            return AVCaptureVideoOrientation.portrait
        case .landscapeRight:
            orientation = .up
            return AVCaptureVideoOrientation.landscapeRight
        case .landscapeLeft:
            orientation = .down
            return AVCaptureVideoOrientation.landscapeLeft
        case .portraitUpsideDown:
            orientation = .left
            return AVCaptureVideoOrientation.portraitUpsideDown
        default:
            orientation = .right
            return AVCaptureVideoOrientation.portrait
        }
    }
    
}
