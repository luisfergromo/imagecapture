//
//  PreviewPhotoContairnerView.swift
//  app01
//
//  Created by Luis Romo on 7/4/19.
//  Copyright © 2019 Luis Romo. All rights reserved.
//

import UIKit
import Photos
var finalImage = UIImage()

class PreviewPhotoContairnerView: UIView{
    
    let previewImageView:UIImageView={
        let iv = UIImageView()
        return iv
    }()
    let saveButton:UIButton = {
        let btn = UIButton()
        //        btn.setImage(#imageLiteral(resourceName: "save_shadow"), for: .normal)
        btn.setTitle("Save", for: .normal)
        btn.addTarget(self, action: #selector(usePhoto), for: .touchUpInside)
        return btn
    }()
    @objc func usePhoto(){
        //        @objc func handleSave() {
        //        print("saving data")
        guard let previewImage = previewImageView.image else {return}
        let library = PHPhotoLibrary.shared()
        library.performChanges({
            PHAssetChangeRequest.creationRequestForAsset(from: previewImage)
        }) { (success, err) in
            if let err = err{
                print("Failed to save image to photo library", err)
            }
            print("Successfully saved image to library")
            finalImage = previewImage
        }
    }
    //    }
    let cancelBtn:UIButton={
        let btn = UIButton(type: .system)
        //        btn.setImage(#imageLiteral(resourceName: "cancel_shadow"), for: .normal)
        btn.setTitle("Discard", for: .normal)
        btn.addTarget(self, action: #selector(handleCancel), for: .touchUpInside)
        return btn
    }()
    @objc func handleCancel() {
        self.removeFromSuperview()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = .yellow
        addSubview(previewImageView)
        addSubview(cancelBtn)
        addSubview(saveButton)
        previewImageView.centerInSuperview()
        previewImageView.anchor(top: nil, leading: nil, bottom: nil, trailing: nil, padding: .init(top: 0, left: 0, bottom: 0, right: 0), size: .init(width: 300, height: 300))
        cancelBtn.anchor(top: topAnchor, leading: leadingAnchor, bottom: nil, trailing: nil, padding: .init(top: 20, left: 12, bottom: 0, right: 0), size: .init(width: 50, height: 50))
        saveButton.anchor(top: nil, leading: leadingAnchor, bottom: bottomAnchor, trailing: nil, padding: .init(top: 0, left: 20, bottom: 20, right: 0))
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
