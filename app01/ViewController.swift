//
//  ViewController.swift
//  app01
//
//  Created by Luis Romo on 7/2/19.
//  Copyright © 2019 Luis Romo. All rights reserved.
//

import UIKit
import AVFoundation

//var capturedImage = UIImage(named: "imagen")
var capturedImage = finalImage

class ViewController: UIViewController{
    
    
    let camera: UIButton = {
        let btn = UIButton(type: .system)
        btn.setTitle("Take Photo", for: .normal)
        btn.translatesAutoresizingMaskIntoConstraints = false
        //    btn.backgroundColor = .gray
        btn.addTarget(self, action: #selector(cameraView), for: .touchUpInside)
        return btn
    }()
    let imageViewFinal: UIImageView = {
//        guard let imageCaptured = previewImageView.image else {return}
        let img = UIImageView()
        img.image = capturedImage 
        return img
    }()
    
    @objc func cameraView() {
        let cameraController = CameraController()
        present(cameraController, animated: true, completion: nil)
    }
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask{
        get{
            return  .all
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
//        view.addSubview(CollectionView)
        view.addSubview(camera)
        view.addSubview(imageViewFinal)
        camera.anchor(top: view.safeAreaLayoutGuide.topAnchor, leading: view.leadingAnchor, bottom:nil, trailing: view.trailingAnchor)
        imageViewFinal.anchor(top: camera.bottomAnchor, leading: view.leadingAnchor, bottom: nil, trailing: view.trailingAnchor)
    }
//    override func willTransition(to newCollection: UITraitCollection, with coordinator: UIViewControllerTransitionCoordinator) {
//
//    }
}
